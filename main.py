import pygame
import neat
import time
import os
import random

pygame.font.init()

def load_image(filename: str):
    return pygame.transform.scale2x(pygame.image.load(
        os.path.join('imgs', filename)))


WIN_WIDTH = 500
WIN_HEIGHT = 700

BIRD_IMAGES = [
    load_image(f'bird{x}.png')
    for x in range(1, 4)
]
PIPE_IMAGE = load_image('pipe.png')
BASE_IMAGE = load_image('base.png')
BG_IMAGE = load_image('bg.png')
STAT_FONT = pygame.font.SysFont('arial', 50)




class Bird:
    imgs = BIRD_IMAGES
    max_rotation = 25
    rotation_velocity = 20
    animation_time = 5

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.tilt = 0
        self.tick_count = 0
        self.velocity = 0
        self.height = self.y
        self.image_count = 0
        self.image = self.imgs[0]

    def jump(self):
        self.velocity = -10.5
        self.tick_count = 0
        self.height = self.y

    def move(self):
        self.tick_count += 1
        displacement = self.velocity * self.tick_count + 1.5 * self.tick_count ** 2
        displacement = min(displacement, 16)
        if displacement < 0:
            displacement -= 2
        self.y += displacement
        if displacement < 0 or self.y < self.height + 50:
            if self.tilt < self.max_rotation:
                self.tilt = self.max_rotation
        else:
            if self.tilt > -90:
                self.tilt -= self.rotation_velocity

    def draw(self, window):
        self.image_count += 1
        if self.image_count < self.animation_time:
            self.image = self.imgs[0]
        elif self.image_count < self.animation_time*2:
            self.image = self.imgs[1]
        elif self.image_count < self.animation_time*3:
            self.image = self.imgs[2]
        elif self.image_count < self.animation_time * 4:
            self.image = self.imgs[1]
        else:
            self.image = self.imgs[0]
            self.image_count = 0

        if self.tilt < -80:
            self.image = self.imgs[1]
            self.image_count = self.animation_time * 2

        rotated_image = pygame.transform.rotate(self.image, self.tilt)
        new_rect = rotated_image.get_rect(center=self.image.get_rect(topleft= (self.x, self.y)).center)
        window.blit(rotated_image, new_rect.center)

    def get_mask(self):
        return pygame.mask.from_surface(self.image)


class Pipe:
    gap = 200
    velocity = 5
    pipe_top = pygame.transform.flip(PIPE_IMAGE, False, True)
    pipe_bottom = PIPE_IMAGE

    def __init__(self, x, y=None):
        self.x = x
        self.height = 0
        self.top = 0
        self.bottom = 0
        self.passed = False
        self.set_height(y)

    def set_height(self, y=None):
        self.height = y or random.randrange(50, 450)
        self.top = self.height - self.pipe_top.get_height()
        self.bottom = self.height + self.gap

    def move(self):
        self.x -= self.velocity

    def draw(self, win):
        win.blit(self.pipe_top, (self.x, self.top))
        win.blit(self.pipe_bottom, (self.x, self.bottom))

    def collide(self, bird):
        bird_mask = bird.get_mask()
        top_mask = pygame.mask.from_surface(self.pipe_top)
        bottom_mask = pygame.mask.from_surface(self.pipe_bottom)

        top_offset = (self.x-bird.x, self.top-round(bird.y))
        bottom_offset = (self.x-bird.x, self.bottom-round(bird.y))

        top_point = bird_mask.overlap(top_mask, top_offset)
        bottom_point = bird_mask.overlap(bottom_mask, bottom_offset)

        return top_point or bottom_point


class Base:
    velocity = 5
    width = BASE_IMAGE.get_width()
    image = BASE_IMAGE

    def __init__(self, y):
        self.y = y
        self.x1 = 0
        self.x2 = self.width

    def move(self):
        self.x1 -= self.velocity
        self.x2 -= self.velocity

        if self.x1 + self.width < 0:
            self.x1 = self.x2 + self.width

        if self.x2 + self.width < 0:
            self.x2 = self.x1 + self.width

    def draw(self, window):
        window.blit(self.image, (self.x1, self.y))
        window.blit(self.image, (self.x2, self.y))


def draw_window(window, birds, pipes, base, score, gen):
    window.blit(BG_IMAGE, (0,0))
    for bird in birds:
        bird.draw(window)
    for pipe in pipes:
        pipe.draw(window)
    base.draw(window)
    text = STAT_FONT.render(f'Score: {score}', False, (255, 255, 255))
    window.blit(text, (WIN_WIDTH-10-text.get_width(), 10))
    text = STAT_FONT.render(f'Gen: {gen}', False, (255, 255, 255))
    window.blit(text, (10, 10))
    pygame.display.update()

gen = 0

def eval_genomes(genomes, config):
    global gen
    nets = []
    birds = []
    ge = []
    gen += 1
    for _, g in genomes:
        net = neat.nn.FeedForwardNetwork.create(g, config)
        nets.append(net)
        bird = Bird(round(WIN_WIDTH/2)-BIRD_IMAGES[0].get_width(), round(WIN_HEIGHT/2)-BIRD_IMAGES[0].get_height())
        birds.append(bird)
        g.fitness = 0
        ge.append(g)



    base = Base(WIN_HEIGHT-100)
    pipes = [Pipe(700)]
    win = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT))
    clock = pygame.time.Clock()
    score = 0
    while True:
        # clock.tick(30)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                break
        # bird.move()
        base.move()

        pipe_ind = 0
        if len(birds) > 0:
            if len(pipes) > 0 and birds[0].x > pipes[0].x + pipes[0].pipe_top.get_width():
                pipe_ind = 1
        else:
            break


        for x, bird in enumerate(birds):
            bird.move()
            ge[x].fitness += 0.1
            # output = nets[x].activate((bird.y, abs(bird.y-pipes[pipe_ind].height), abs(bird.y-pipes[pipe_ind].bottom)))
            output = nets[x].activate((bird.y, pipes[pipe_ind].height, pipes[pipe_ind].bottom))
            if output[0] > 0:
                bird.jump()

        removed_pipes = []
        add_pipe = False
        for pipe in pipes:
            for x, bird in enumerate(birds):
                if pipe.collide(bird):
                    ge[x].fitness -= 1
                    birds.pop(x)
                    nets.pop(x)
                    ge.pop(x)

                if not pipe.passed and pipe.x < bird.x:
                    pipe.passed = True
                    add_pipe = True
            if pipe.x + pipe.pipe_top.get_width() < 0:
                removed_pipes.append(pipe)
            pipe.move()
        if add_pipe:
            for g in ge:
                g.fitness += 5
            score += 1
            pipes.append(Pipe(WIN_WIDTH))
        for pipe in removed_pipes:
            pipes.remove(pipe)
        for x, bird in enumerate(birds):
            if bird.y + bird.image.get_height() >= WIN_HEIGHT or bird.y < 0:
                birds.pop(x)
                nets.pop(x)
                ge.pop(x)
        draw_window(win, birds, pipes, base, score, gen)


def run(config_path):
    config = neat.config.Config(
        neat.DefaultGenome,
        neat.DefaultReproduction,
        neat.DefaultSpeciesSet,
        neat.DefaultStagnation,
        config_path
    )
    p = neat.population.Population(config)
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(1))
    winner = p.run(eval_genomes, 50)


if __name__ == '__main__':
    cdir = os.path.dirname(__file__)
    config_path = os.path.join(cdir, 'config-feedfoward.txt')
    run(config_path)
